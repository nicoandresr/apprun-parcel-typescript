import app, { Component } from 'apprun';
import BEM, { Modifier } from 'js-bem';

class Home extends Component {
  styles = new BEM({
    heading: {
      color: 'blue',
      backgroundColor: 'gray'
    }
  });

  state = 'Hello world!!';

  view = state => (
    <header className={this.styles.heading.bem()}>
      <h1>{state}</h1>
    </header>
  );

  update = {
    '#': state => state
  };
}

export default Home;
