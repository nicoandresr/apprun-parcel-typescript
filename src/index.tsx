import app, { Component } from 'apprun';

import Home from './components/home';

const target = document.getElementById('app');

new Home().mount(target);
